#import "sc_listener_client_sampleAppDelegate.h"
#import "sc_listener_client_sampleViewController.h"
#import "SCListener.h"



@implementation sc_listener_client_sampleAppDelegate

@synthesize window;
@synthesize viewController;
@synthesize timer;
@synthesize mitimer;

bool toggle;



- (void)applicationDidFinishLaunching:(UIApplication *)application {    

    
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    [prefs setFloat:0.06 forKey:@"iker_intervalo"];
    
    if ([prefs objectForKey:@"iker_intervalo"] == nil) {
        NSDictionary *values = [NSDictionary dictionaryWithObjectsAndKeys:@"0.5", @"iker_intervalo", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:values ];
    }

    
    if ([prefs objectForKey:@"iker_potencia"] == nil) {
        NSDictionary *values = [NSDictionary dictionaryWithObjectsAndKeys:@"0.017", @"iker_potencia", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:values ];
    }
    
    if ([prefs objectForKey:@"iker_voz"] == nil) {
        NSDictionary *values = [NSDictionary dictionaryWithObjectsAndKeys:@"0.85", @"iker_voz", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:values ];
    }    
    
    // getting an Float
    float myIntervalo = [prefs floatForKey:@"iker_intervalo"];

    NSLog(@"Interbaloa da: %f",myIntervalo);
    
	//float myPotencia = [prefs floatForKey:@"iker_potencia"];
	
    //AudioSessionInitialize(NULL,NULL,NULL,NULL);
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
//    NSError *nsError;
//    [[AVAudioSession sharedInstance]
//     setCategory:AVAudioSessionCategoryPlayback error:&nsError];
    
    [[SCListener sharedListener] listen];

	timer = [NSTimer scheduledTimerWithTimeInterval: myIntervalo target: self selector: @selector(tick:) userInfo:nil repeats: YES];

	
	
    [[ UIApplication sharedApplication ] setIdleTimerDisabled: YES ]; // HACEMOS QUE LA PANTALLA NO SE APAGUE
    
    // Override point for customization after app launch    
    //[window addSubview:viewController.view];
    // iOS7 fix
    [self.window setRootViewController:viewController];
    [window makeKeyAndVisible];
}

- (void)tick: (NSTimer*) _timer {
//	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//	float myIntervalo   = [prefs floatForKey:@"iker_intervalo"];
    float myIntervalo   = 0.05;
//	float myPotencia    = [prefs floatForKey:@"iker_potencia"];
    float myPotencia    = 0.017;
//	float myVoz         = [prefs floatForKey:@"iker_voz"];
    float myVoz         = 0.85;
    
//    NSLog(@"Intervalo: %f",myIntervalo);
//    NSLog(@"Potencia: %f",myPotencia);
//    NSLog(@"Voz: %f",myVoz);    
          
    
	Float32 peak_power      = [[SCListener sharedListener] peakPower];
	Float32 average_power   = [[SCListener sharedListener] averagePower];
	Float32 frequency       = [[SCListener sharedListener] frequency];
    
    if (![[SCListener sharedListener] isListening]) // If listener has paused or stopped...
    {   } 
    else 
    {
        //
        // 05-05-2011 Iker Ibarguren
        //
        // Fondo txuria ezarri ahotsik detektatzen ez duenean
        //
        
        [viewController.btnReproducir setImage:[UIImage imageNamed:@"txuria.png"] forState: UIControlStateNormal];
        
        [mitimer invalidate];
        
        mitimer = nil;
    
    }
	
	if (average_power < myPotencia)		
	
        return;
	
    if (peak_power > myVoz) {
        
        viewController.imgVozDetectada.hidden = NO;
        [[SCListener sharedListener] stop];
                    mitimer = [NSTimer scheduledTimerWithTimeInterval: 0.8
                                                       target: self
                                                     selector: @selector(toggleButtonImage:)
                                                     userInfo: nil
                                                      repeats: YES];
    }
        
  
  
    //
    //  15-05-2011 Ahotsa detektatzen
    //
	
	[viewController.peak_power_label    setText: [NSString stringWithFormat: @"Potencía de pico: %f", peak_power]];
	
    [viewController.average_power_label setText: [NSString stringWithFormat: @"Potencia media: %f", average_power]];
	
    [viewController.freq_label          setText: [NSString stringWithFormat: @"Frecuencia: %f", frequency]];
	
    [viewController.fft_view updateFreqs: [[SCListener sharedListener] freq_db] data2: [[SCListener sharedListener] freq_db_harmonic]];
	
    [viewController.fft_view    setNeedsDisplay];
	
    [viewController.lblIntervalo        setText: [NSString stringWithFormat: @"Sg: %f", myIntervalo]];
	
    [viewController.lblPotencia         setText: [NSString stringWithFormat: @"Filtro: %f", myPotencia]];

}

- (void)dealloc {

    [viewController release];
    
    [timer release];
    
    [mitimer release];
    
    [window release];
    
    [super dealloc];

}

- (void)toggleButtonImage:(NSTimer*) _mitimer

{
    
    if(toggle)
   
    {
    
        [viewController.btnReproducir setImage:[UIImage imageNamed:@"txuria.png"] forState: UIControlStateNormal];
    
    }
    
    else 
    
    {
    
        [viewController.btnReproducir setImage:[UIImage imageNamed:@"berdea.png"] forState: UIControlStateNormal];
    
    }
    
    toggle = !toggle;
}

@end
