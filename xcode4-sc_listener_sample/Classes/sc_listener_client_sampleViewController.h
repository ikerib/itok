//
//  sc_listener_client_sampleViewController.h
//  sc_listener_client_sample
//
//  Created by Jared Kells on 10/02/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFTView.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>


@interface sc_listener_client_sampleViewController : UIViewController 
<AVAudioPlayerDelegate> {
	SystemSoundID sampleSoundID;
	AVAudioPlayer *player;
	
	FFTView *fft_view;
	UILabel *peak_power_label;
	UILabel *average_power_label;
	UILabel *freq_label;
	UILabel *lblIntervalo;
	UILabel *lblPotencia;
    IBOutlet UIImageView *imgHablando;
    IBOutlet UIImageView *imgVozDetectada;
  //  UIButton *btnOcultar;
    IBOutlet UIButton *btnOcultar;
    IBOutlet UIButton *btnReproducir;

}

@property (nonatomic, retain) IBOutlet FFTView *fft_view;
@property (nonatomic, retain) IBOutlet UILabel* peak_power_label;
@property (nonatomic, retain) IBOutlet UILabel* average_power_label;
@property (nonatomic, retain) IBOutlet UILabel* freq_label;
@property (nonatomic, retain) IBOutlet UILabel* lblIntervalo;
@property (nonatomic, retain) IBOutlet UILabel* lblPotencia;
@property (nonatomic, retain) IBOutlet UIImageView *imgHablando;
@property (nonatomic, retain) IBOutlet UIImageView *imgVozDetectada;
@property (nonatomic, retain) IBOutlet UIButton *btnOcultar;
@property (nonatomic, retain) IBOutlet UIButton *btnReproducir;
@property (nonatomic, retain) IBOutlet AVAudioPlayer *player;

- (IBAction) playSound: (id) sender;
- (IBAction) playSong: (id) sender;
- (IBAction)ocultar:(id)sender;


@end

