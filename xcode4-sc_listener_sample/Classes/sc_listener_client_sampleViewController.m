//
//  sc_listener_client_sampleViewController.m
//  sc_listener_client_sample
//
//  Created by Jared Kells on 10/02/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "sc_listener_client_sampleViewController.h"


@implementation sc_listener_client_sampleViewController

@synthesize average_power_label;
@synthesize peak_power_label;
@synthesize freq_label;
@synthesize fft_view;
@synthesize lblIntervalo;
@synthesize lblPotencia;
@synthesize imgHablando;
@synthesize imgVozDetectada;
@synthesize btnOcultar;
@synthesize btnReproducir;
@synthesize player;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	//NSLog(@"InView did load");
	
	//AudioServicesCreateSystemSoundID((CFURLRef)[NSURL fileURLWithPath:[[NSBundle mainBundle] 
	//																   pathForResource: @"sound" ofType:@"aif"]],
	//								 &sampleSoundID);
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}


- (BOOL) shouldAutorotate {
    return YES;
}


//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}


// IOS7 deshabilitamos lo de abajo y activamos las tres de arriba para el correcto funcionamiento del layout
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
//    if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
//        //return YES to allow it or NO to forbid it
//        return NO;
//    if (toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
//        //return YES to allow it or NO to forbid it
//        return YES;
//    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft)
//    //    return YES to allow it or NO to forbid it
//        return NO;
//    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
//        //return YES to allow it or NO to forbid it
//        return NO;
//    return NO; // Unknown value
//}



-(IBAction) playSound:(id) sender {
	
	AudioServicesPlaySystemSound(sampleSoundID);
}

-(IBAction) playSong:(id) sender {
	[[SCListener sharedListener] stop];
	NSString *voz		= [[NSUserDefaults standardUserDefaults] stringForKey:@"iker_audio"];
    NSError *error = nil;
    NSString *soundFilePath=nil;
    NSLog(@"Voz da: %@",voz);

    soundFilePath =[[NSBundle mainBundle] pathForResource:voz ofType:@"mp3"];
 
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
    AVAudioPlayer *newPlayer =[[AVAudioPlayer alloc] initWithContentsOfURL: fileURL error: nil];
    [fileURL release];
    
    self.player = newPlayer;
    [newPlayer release];
    
    [player prepareToPlay];
    [player setDelegate: self];

	if(error != NULL) {		
//		NSLog([error description]);
		[error release];
	}
    imgVozDetectada.hidden =YES;
	imgHablando.hidden = NO;
    
  
    [self.player setVolume: 2.0f];
    
    if (player.playing == YES)
    {
        //[self.player pause];
        
        // if stopped or paused, start playing
    } else {
        [self.player play];
    }
    
        
}


- (IBAction)ocultar:(id)sender {
    imgVozDetectada.hidden =YES;
	imgHablando.hidden = YES;
	[[SCListener sharedListener] listen];
}


-(void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) theplayer
					   successfully:(BOOL) flag {
	
	[[SCListener sharedListener] listen];
    imgHablando.hidden = YES;
	
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [self setBtnOcultar:nil];
    [btnOcultar release];
    btnOcultar = nil;
    [self setImgVozDetectada:nil];
    [self setImgHablando:nil];
    [imgVozDetectada release];
    imgVozDetectada = nil;
    [imgHablando release];
    imgHablando = nil;
    player=nil;
    // Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[player release];
    [imgHablando release];
    [imgVozDetectada release];
    [imgHablando release];
    [imgVozDetectada release];
    [btnOcultar release];
    [btnOcultar release];
    [btnReproducir release];
    [super dealloc];
}

@end
