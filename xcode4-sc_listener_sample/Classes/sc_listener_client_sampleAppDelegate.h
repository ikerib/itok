
#import <UIKit/UIKit.h>


@class sc_listener_client_sampleViewController;

@interface sc_listener_client_sampleAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow *window;
	NSTimer* timer;
    NSTimer *mitimer;
    sc_listener_client_sampleViewController *viewController;
    

}
@property (nonatomic, retain) NSTimer* timer;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet sc_listener_client_sampleViewController *viewController;
@property (nonatomic, retain) IBOutlet NSTimer *mitimer;



@end

